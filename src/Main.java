import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.sun.jndi.url.corbaname.corbanameURLContextFactory;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application implements Runnable {

	GraphicsContext gc;
	int x = 50;
	int y = 50;
	Agent myAgent;
	int[][] gridData = { 
			{ 0, 1, 1, 1, 1, 0, 1, 0, 0, 1 }, 
			{ 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 },
			{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 }, 
			{ 1, 1, 0, 0, 0, 1, 0, 1, 1, 0 }, 
			{ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 },
			{ 1, 0, 0, 0, 0, 0, 0, 1, 0, 1 }, 
			{ 1, 1, 0, 1, 0, 1, 0, 0, 0, 0 }, 
			{ 0, 0, 0, 1, 0, 1, 0, 1, 0, 0 },
			{ 1, 0, 1, 1, 0, 1, 0, 0, 0, 1 }, 
			{ 0, 0, 1, 1, 1, 1, 0, 0, 1, 1 } 
	};

	double[][] Q = new double[100][4];
	double[][] R = new double[100][4];
	Random rand = new Random();
	double gamma = 0.3;
	int goalState = 39;
	int startState = 0;
	int currentAgentState = startState;
	int episode = 0;
	int moves = 0;
	Thread thread;

	Label labelXY;
	Label labelState;
	Label labelGrid;
	Canvas canvas;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		BorderPane rootNode = new BorderPane();
		canvas = new Canvas(500, 500);
		labelXY = new Label();
		labelState = new Label();
		labelGrid = new Label();
		Scene scene = new Scene(rootNode, 700, 500);

		VBox box = new VBox();
		box.getChildren().addAll(labelXY, labelState, labelGrid);
		rootNode.setCenter(box);
		rootNode.setLeft(canvas);
		stage.setTitle("Agents");
		stage.sizeToScene();
		stage.setScene(scene);
		stage.show();

		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent event) {
				if (event.getCode().equals(KeyCode.UP)) {
					MoveAgent(0);
				}
				if (event.getCode().equals(KeyCode.DOWN)) {
					MoveAgent(1);
				}
				if (event.getCode().equals(KeyCode.LEFT)) {
					MoveAgent(2);
				}
				if (event.getCode().equals(KeyCode.RIGHT)) {
					MoveAgent(3);
				}
				if (event.getCode().equals(KeyCode.G)) {
					// do {
					do {
						AgentTick();
					} while (currentAgentState != goalState);
					// printTable(Q);
					episode++;
					System.out.println(episode + " episode done");
					System.out.println("Moves: " + moves);
					System.out.println("Success state: " + CoordinatesToState(myAgent));

					moves = 0;
					myAgent = StateToCoordinates(startState);
					currentAgentState = startState;
					// } while (episode != 1500);
				}
				if (event.getCode().equals(KeyCode.W)) {
					moves = 0;
					myAgent.x = 0;
					myAgent.y = 9;
					reDrawNewGrid();
				}
				if (event.getCode().equals(KeyCode.D)) {
					AgentTick();
				}
			}
		});

		gc = canvas.getGraphicsContext2D();
		myAgent = StateToCoordinates(startState);

		DrawGrid(gc);
		Init_R();
		Init_Q();
		// printTable(R);
		// printTable(R);
		// printGrid();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}

	public int maxQAction(int state) {
		double max = Q[state][0];
		int action = 0;
		if (getRandomNumberInRange(1, 10) == 1) {
			return getRandomNumberInRange(0, 3);
		}
		for (int a = 1; a < 4; a++) {
			if (max < Q[state][a]) {
				max = Q[state][a];
				action = a;
			}
		}
		// if (max == 0.000000000000) {
		// action = getRandomNumberInRange(0, 3);
		// }
		return action;
	}

	public double updateQ(int state, int action, int nextState) {
		double max = Q[nextState][0];
		for (int a = 0; a < 4; a++) {
			if (max < Q[nextState][a]) {
				max = Q[nextState][a];
			}
		}
		// System.out.println("Max: " + max);
		// System.out.println("R: " + (R[state][action] + gamma * max));
		return R[state][action] + gamma * max;
	}

	public void printGrid() {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				System.out.println("grid[" + i + "][" + j + "] = " + gridData[i][j]);
			}
		}
	}

	public void AgentTick() {
		int previousState = CoordinatesToState(myAgent);
		int action = maxQAction(previousState);
		MoveAgent(action);
		Q[previousState][action] = updateQ(previousState, action, CoordinatesToState(myAgent));
	}

	private static int getRandomNumberInRange(int min, int max) {
		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	private void Init_Q() {
		for (int i = 0; i < 10 * 10; i++) {
			for (int j = 0; j < 4; j++) {
				Q[i][j] = 0;
			}
		}
	}

	private void Init_R() {
		Agent dummyAgent;
		Agent newDummyAgent;
		for (int i = 0; i < 10 * 10; i++) {
			for (int j = 0; j < 4; j++) {
				R[i][j] = -1;
			}
		}
		for (int i = 0; i < 10 * 10; i++) {
			dummyAgent = StateToCoordinates(i);
			int x = dummyAgent.x;
			int y = dummyAgent.y;

			// System.out.println("State= " + i + " x: " + x + " y: " + y);
			// System.out.println("gridData = " + gridData[y][x]);
			if (gridData[y][x] == 0) {
				if (x > 0 && y > 0 && x < 9 && y < 9) {
					R[i][0] = gridData[y - 1][x];
					R[i][1] = gridData[y + 1][x];
					R[i][2] = gridData[y][x - 1];
					R[i][3] = gridData[y][x + 1];
				}
				if (x == 0 && y > 0 && y < 9) {
					R[i][0] = gridData[y - 1][x];
					R[i][1] = gridData[y + 1][x];
					R[i][3] = gridData[y][x + 1];
				}
				if (y == 0 && x > 0 && x < 9) {
					R[i][1] = gridData[y + 1][x];
					R[i][2] = gridData[y][x - 1];
					R[i][3] = gridData[y][x + 1];
				}
				if (x == 9 && y > 0 && y < 9) {
					R[i][0] = gridData[y - 1][x];
					R[i][1] = gridData[y + 1][x];
					R[i][2] = gridData[y][x - 1];
				}
				if (y == 9 && x > 0 && x < 9) {
					R[i][0] = gridData[y - 1][x];
					R[i][2] = gridData[y][x - 1];
					R[i][3] = gridData[y][x + 1];
				}
				if (x == 0 && y == 0) {
					R[i][1] = gridData[y + 1][x];
					R[i][3] = gridData[y][x + 1];
				}
				if (x == 0 && y == 9) {
					R[i][0] = gridData[y - 1][x];
					R[i][3] = gridData[y][x + 1];
				}
				if (x == 9 && y == 0) {
					R[i][1] = gridData[y + 1][x];
					R[i][2] = gridData[y][x - 1];
				}
				if (x == 9 && y == 9) {
					R[i][0] = gridData[y - 1][x];
					R[i][2] = gridData[y][x - 1];
				}
			}
		}

		dummyAgent = StateToCoordinates(goalState);

		if (goalState + 1 < 100) {
			newDummyAgent = StateToCoordinates(goalState + 10);
			if (newDummyAgent.y < 10 && gridData[newDummyAgent.y][newDummyAgent.x] == 0) {
				R[goalState + 10][0] = 100;
			}
		}

		if (goalState - 1 > 0) {
			newDummyAgent = StateToCoordinates(goalState - 10);
			if (newDummyAgent.y > 0 && gridData[newDummyAgent.y][newDummyAgent.x] == 0) {
				R[goalState - 10][1] = 100;
			}
		}

		if (goalState + 1 < 100) {
			newDummyAgent = StateToCoordinates(goalState + 1);
			if (dummyAgent.y == newDummyAgent.y && gridData[newDummyAgent.y][newDummyAgent.x] == 0) {
				R[goalState + 1][2] = 100;
			}
		}

		if (goalState - 1 > 0) {
			newDummyAgent = StateToCoordinates(goalState - 1);
			if (dummyAgent.y == newDummyAgent.y && gridData[newDummyAgent.y][newDummyAgent.x] == 0) {
				R[goalState - 1][3] = 100;
			}
		}

		for (int i = 0; i < 10 * 10; i++) {
			for (int j = 0; j < 4; j++) {
				if (R[i][j] == 1) {
					R[i][j] = -1;
				}
			}
		}

		// R[15][0] = 100;
	}

	private void printTable(double[][] table) {
		System.out.println("     0    1    2    3");
		for (int i = 0; i < 10 * 10; i++) {
			if (i < 10) {
				System.out.print("" + i + "  |");
			} else {
				System.out.print("" + i + " |");
			}
			for (int j = 0; j < 4; j++) {
				System.out.print("" + table[i][j] + "  ");
			}
			System.out.println("");
		}
	}

	public int CoordinatesToState(Agent ag) {
		int state = 0;
		state = ag.y * 10 + ag.x;
		return state;
	}

	public Agent StateToCoordinates(int state) {
		Agent ag = new Agent(0, 0);
		ag.x = state % 10;
		ag.y = state / 10;

		return ag;
	}

	public void MoveAgent(int a) {
		moves++;
		switch (a) {
		// 0 = UP, 1 = Down, 2 = Left, 3 = Right;
		case 0:
			if (myAgent.y - 1 >= 0 && gridData[myAgent.y - 1][myAgent.x] != 1)
				myAgent.y -= 1;
			textToLabel(myAgent);
			// System.out.println("Action taken: UP");
			currentAgentState = CoordinatesToState(myAgent);
			break;
		case 1:
			if (myAgent.y + 1 <= 9 && gridData[myAgent.y + 1][myAgent.x] != 1)
				myAgent.y += 1;
			textToLabel(myAgent);
			// System.out.println("Action taken: Down");
			currentAgentState = CoordinatesToState(myAgent);
			break;
		case 2:
			if (myAgent.x - 1 >= 0 && gridData[myAgent.y][myAgent.x - 1] != 1)
				myAgent.x -= 1;
			textToLabel(myAgent);
			// System.out.println("Action taken: Left");
			currentAgentState = CoordinatesToState(myAgent);
			break;
		case 3:
			if (myAgent.x + 1 <= 9 && gridData[myAgent.y][myAgent.x + 1] != 1)
				myAgent.x += 1;
			textToLabel(myAgent);
			// System.out.println("Action taken: Right");
			currentAgentState = CoordinatesToState(myAgent);
			break;
		}
		reDrawNewGrid();
	}

	public void textToLabel(Agent ag) {
		labelXY.setText("X: " + ag.x + " Y: " + ag.y);
		labelState.setText("State: " + CoordinatesToState(ag));
		labelGrid.setText("Grid:" + gridData[ag.y][ag.x]);
	}

	public void reDrawNewGrid() {
		gc.clearRect(0, 0, 500, 500);
		DrawGrid(gc);
		gc.fillOval(myAgent.x * 50 + 10, myAgent.y * 50 + 10, 30, 30);
	}

	public void drawQTableToGrid(GraphicsContext g) {
		g.setLineWidth(1);
		g.setStroke(Color.BLACK);
		for (int i = 0; i < 9; i++) {

		}
	}

	public void DrawGrid(GraphicsContext g) {
		int i = 0;
		int j = 0;
		g.setLineWidth(2);
		g.setStroke(Color.BLACK);
		for (i = 0; i < 11; i++) {
			g.strokeLine(0, i * y, 500, i * y);
			for (j = 0; j < 11; j++) {
				if (i < 10 && j < 10 && gridData[i][j] == 1) {
					g.setFill(Color.BLUE);
					g.fillRect(j * x + 1, i * y + 1, 48, 48);
				}
				g.strokeLine(j * x, 0, j * x, 500);
			}
		}
		Agent dummy = StateToCoordinates(startState);
		g.setFill(Color.GREEN);
		g.fillRect(dummy.x * 50 + 1, dummy.y * 50 + 1, 48, 48);

		dummy = StateToCoordinates(goalState);
		g.setFill(Color.RED);
		g.fillRect(dummy.x * 50 + 1, dummy.y * 50 + 1, 48, 48);

		g.setFill(Color.CHARTREUSE);
		g.fillOval(myAgent.x * 50 + 10, myAgent.y * 50 + 10, 30, 30);
	}
}
